package polymorphismhw;

public class Paises implements GovernmentProcesses {

    public String CountryName;
    double TaxRate, TaxToPay;
    private int Income;

    public Paises(String CountryName, double TaxRate) {
        this.CountryName = CountryName;
        this.TaxRate = TaxRate;
    }

    public int getIncome() {
        return Income;
    }

    public void setIncome(int Income) {
        this.Income = Income;
    }

    public double getTaxToPay() {
        return TaxToPay;
    }

    public void setTaxToPay(double TaxToPay) {
        this.TaxToPay = TaxToPay;
    }

    public void TaxCalculation() {
        TaxToPay = (this.Income * this.TaxRate) * 12;
        System.out.println("Yearly, " + this.CountryName + " population pays: $" + this.TaxToPay + " as taxes");

    }

    public void newBorns(int newborns) {
        System.out.println(this.CountryName +" has "+newborns + "newborns today.");
    }
    
    public void newBorns(boolean x){
    if (x==true)
    System.out.println("In this country there are a lot of newborns.");
    else 
    System.out.println("Not too many people procreates in this country.");
    }

}
