
package polymorphismhw;

 class FlyingPokemon extends Pokemon {
    
    void flyingAttack(String fAttack){
        System.out.println("I'm using my flying attack "+fAttack);
    }
    void evolve() {
        System.out.println("This flying pokemon just evolved");
    }
     
    
}
