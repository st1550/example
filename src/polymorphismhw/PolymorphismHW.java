
package polymorphismhw;

import java.util.Scanner;


public class PolymorphismHW {

    public static void main(String[] args) {
        int income;
        Scanner sc = new Scanner(System.in);
        
        //Interface
        Paises mexico = new Paises("Mexico" , .16);
        System.out.print("Provide monthly average income for "+mexico.CountryName+" population: ");
        income=sc.nextInt();
        mexico.setIncome(income);   
        mexico.TaxCalculation();
        
        //Abstract Class
        FlyingPokemon zapdos = new FlyingPokemon() {};
        zapdos.getOutFromPokeball("\n\nZapdos");
        zapdos.sayType("Electic");
        zapdos.flyingAttack("Thunderwave");
        zapdos.evolve();
        
        //Overload
        Paises northIreland = new Paises("North Ireland" , .11);
        System.out.print("\n\nProvide monthly average income for "+northIreland.CountryName+" population: ");
        income=sc.nextInt();
        northIreland.setIncome(income);   
        northIreland.TaxCalculation();
        northIreland.newBorns(44);
        northIreland.newBorns(false);System.out.println("");
        
        //Overriding
        Laptop asusLap = new Laptop("Asus");
        HpLaptop hpLap = new HpLaptop("HP");
        asusLap.describreYourself();
        asusLap.displayBrand();
        System.out.println("");
        hpLap.describreYourself();
        hpLap.displayBrand();
        
        
        
        
        
        
        
        
     
    }
    
}
