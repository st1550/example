package polymorphismhw;

abstract class Pokemon {

     void getOutFromPokeball(String name) {
        System.out.println(name + "-: I'm getting out from my Pokeball");

    }

     protected void sayType(String type) {
        System.out.println("I'm " + type + " pokemon...");
    }

     abstract void evolve();

}
