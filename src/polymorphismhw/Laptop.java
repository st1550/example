
package polymorphismhw;


public class Laptop {
    
    public String Brand;

    public Laptop(String Brand) {
        this.Brand = Brand;
    }

    Laptop() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    void describreYourself(){
        System.out.println("I'm a nice laptop, I can do awesome tasks");
    }
    
    void displayBrand(){
        System.out.println("I'm a OEM laptop, my brand is: "+this.Brand);
    }
    
}
